###################################################
# VPC Endpoint service
###################################################

resource "aws_vpc_endpoint_service" "this" {
  count = var.create_service ? 1 : 0

  network_load_balancer_arns = [aws_lb.service_nlb[0].arn]

  acceptance_required = var.acceptance_required

  tags = merge(
    var.tags
  )
}


###################################################
# Allowed Principals
###################################################


resource "aws_vpc_endpoint_service_allowed_principal" "this" {
  for_each = var.create_service ? toset(var.allowed_principals) : toset([])

  vpc_endpoint_service_id = aws_vpc_endpoint_service.this[0].id
  principal_arn           = each.value
}
