
###################################################
# Local Formatting for dynamic resource creation
###################################################

locals {
  target_group_attachments = merge(flatten([
    for listener, values in var.service_listener : {
      for attachments in values["target_group"]["target_attachments"] :
      attachments => merge({ listener = listener }, { target_port = values["target_group"]["target_port"] })
    }
    ]
  )...)


  targets = {
    true  = var.service_listener
    false = {}
  }

}

###################################################
# AWS NLB for the VPC Endpoint service
###################################################

resource "aws_lb" "service_nlb" {
  count = var.create_service ? 1 : 0

  name               = "vpcs-${var.service_name}-nlb"
  internal           = true
  load_balancer_type = "network"
  subnets            = length(var.service_subnets_ids) == 0 ? data.aws_subnet_ids.subnets.ids : var.service_subnets_ids

  tags = merge(
    var.tags,
    {
      Name = var.service_name
    },
  )
}

###################################################
# AWS NLB listener for the VPC Endpoint service
###################################################

resource "aws_lb_listener" "nlb_listener" {
  for_each = local.targets[var.create_service]

  load_balancer_arn = aws_lb.service_nlb[0].arn
  port              = each.value.listener_port
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_target_group[each.key].arn
  }

  tags = merge(
    var.tags
  )
}

###########################################################################
# AWS NLB listener Target Groups for the VPC Endpoint service
###########################################################################

resource "aws_lb_target_group" "nlb_target_group" {
  for_each = local.targets[var.create_service]

  name        = "${each.key}-nlb-target"
  port        = each.value.target_group.target_port
  protocol    = "TCP"
  target_type = each.value.target_group.target_type
  vpc_id      = var.vpc_id

  dynamic "health_check" {
    for_each = length(keys(lookup(each.value.target_group, "health_check", {}))) == 0 ? [] : [lookup(each.value.target_group, "health_check", {})]

    content {
      enabled             = lookup(health_check.value, "enabled", null)
      interval            = lookup(health_check.value, "interval", null)
      path                = lookup(health_check.value, "path", null)
      port                = lookup(health_check.value, "port", null)
      healthy_threshold   = lookup(health_check.value, "healthy_threshold", null)
      unhealthy_threshold = lookup(health_check.value, "unhealthy_threshold", null)
      timeout             = lookup(health_check.value, "timeout", null)
      protocol            = lookup(health_check.value, "protocol", null)
      matcher             = lookup(health_check.value, "matcher", null)
    }
  }

  tags = merge(
    var.tags
  )
}

###########################################################################
# AWS Target Groups attachments for the VPC Endpoint service
###########################################################################

resource "aws_lb_target_group_attachment" "service_nlb_https" {
  for_each = var.create_service ? local.target_group_attachments : {}

  target_group_arn = aws_lb_target_group.nlb_target_group[each.value.listener].arn
  target_id        = each.key
  port             = each.value.target_port

}

###########################################################################
# Get the IP address of the Network load balancer
###########################################################################

data "aws_network_interface" "lb" {
  for_each = var.create_service ? aws_lb.service_nlb[0].subnets : toset([])

  filter {
    name   = "description"
    values = ["ELB ${aws_lb.service_nlb[0].arn_suffix}"]
  }

  filter {
    name   = "subnet-id"
    values = [each.value]
  }
}