output "service_name" {
  value = concat(aws_vpc_endpoint_service.this.*.service_name, [""])[0]
}

output "service_type" {
  value = concat(aws_vpc_endpoint_service.this.*.service_type, [""])[0]
}

output "service_nlb_ips" {
  value = formatlist("%s/32", [for eni in data.aws_network_interface.lb : eni.private_ip])
}