variable "vpc_id" {
  default = ""
}

variable "allowed_principals" {
}

variable "create_service" {
  type    = bool
  default = false
}

variable "service_name" {
  default = ""
}

variable "acceptance_required" {
  default = false
}

variable "service_listener" {
}

variable "service_subnets_ids" {
  default = []
}

variable "tags" {
  default = {}
}

variable "application_security_group_ids" {
  default = []
}
